<?php

class Database 
{
    private $host       = 'localhost';
    private $username   = 'root';
    private $password   = '';
    private $dbname     = 'datasaver';
    private $conn       = '';

    public function __construct()
    {
        $this->conn = new mysqli($this->host,$this->username,$this->password,$this->dbname);
    }

    public function insert($table, $para = array())
    {
        $return = array();
        $table_columns  = implode(',', array_keys($para));
        $table_values   = implode("','", $para);
        $query          = "INSERT INTO $table ($table_columns) VALUES('$table_values')";
        $result         = $this->conn->query($query);
        if($result == true)
        {
           $return['status']        = 'success'; 
           $return['html_message']  = 'Data saved successfully'; 
        }
        else
        {
            $return['status']        = 'error'; 
            $return['html_message']  = $this->conn->error;
        }
        return $return;
    }

    public function select($table)
    {
        $count = 1;
        $query = "SELECT * FROM $table";
        $result = $this->conn->query($query);
        if ($result->num_rows > 0) 
        {
            while ($row = $result->fetch_array())
            {
                foreach($row as $key => $value)
                {
                    // if(strlen($value) > 50)
                    // {
                    //     $row[$key] = '<span data-toggle="tooltip" data-bs-placement="bottom" title="'.$value.'">'.substr($value,0,20).'....</span>';
                    // }
                }
                            
                $row[0] = $count++;
                array_push($row, '<div class=""><button onclick = "editdata('.$row['id'].')" data-bs-toggle="modal" data-bs-target="#exampleModal" id="e'.$row['id'].'" class="mr-3 btn btn-primary btn-sm"><i class="fas fa-edit"></i></button>
                <button onclick = "deletedata('.$row['id'].')" data-bs-toggle="modal" data-bs-target="#exampleModal2" id="d'.$row['id'].'" class="my-2 btn btn-danger btn-sm"><i class="fa-solid fa-trash"></i></button></div>');

                $data['data'][] = $row;
            }
        }
        else 
        {
            $data['data'] = array();
        }
        return $data;
    }

    public function selectid($id,$table)
    {
        $query = "SELECT * FROM $table WHERE id = '$id'";
        $result = $this->conn->query($query);
        $data = $result->fetch_assoc();      
        return $data;
    }

    public function edit($table ,$para = array(), $where)
    {
        foreach($para as $key => $value)
        {
            $columns[] = "$key = '$value'";        
        }
        $columns_set = implode(",",$columns);
        $query = "UPDATE $table SET $columns_set WHERE $where";
        $result         = $this->conn->query($query);
        if($result == true)
        {
           $return['status']        = 'success'; 
           $return['html_message']  = 'Data saved successfully'; 
        }
        else
        {
            $return['status']        = 'error'; 
            $return['html_message']  = $this->conn->error;
        }
        return $return; 
    }

    public function delete($id,$table)
    {
        $query = "DELETE FROM $table WHERE id = $id";
        $result         = $this->conn->query($query);
        if($result == true)
        {
           $return['status']        = 'success'; 
           $return['html_message']  = 'Data Deleted successfully'; 
        }
        else
        {
            $return['status']        = 'error'; 
            $return['html_message']  = $this->conn->error;
        }
        return $return;
    }
}