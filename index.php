<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='maximum-scale=1.0, initial-scale=1.0, width=device-width' name='viewport'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/7854769f40.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
    <script src="https://alpha2.ithinklogistics.com/theme2/assets/js/plugins/notifybar/jquery.notifyBar.js"></script>
    <link rel="stylesheet" href="https://alpha2.ithinklogistics.com/theme2/assets/js/plugins/notifybar/css/jquery.notifyBar.css">
    <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.4/css/dataTables.bootstrap5.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/rowreorder/1.2.8/css/rowReorder.dataTables.min.css" rel="stylesheet">
    
    
    <link rel="stylesheet" href="index.css">
    <title>DataSaver</title>
</head>
<body style="background: #f0f2f5;">
    <div class="container-fluid">       
        <div class="row text-center text-primary">
            <div class="card col-lg-12 col-md-12 col-sm-12 col-xs-12 col-md-card shadow-lg p-3 mb-5 bg-white rounded shadow-pop">
                <h2><em>DataSaver</em></h2> 
                <p class="display-7"><em>Easy save</em></p>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="card col-md-4 col-lg-4 col-sm-12 col-xs-12 col-md-card shadow-lg p-3 mb-5 bg-white rounded shadow-pop">
                <div class="card-body text-center">
                    <form id="datasaverform" method="POST" data-parsley-validate>
                        <div class="form-group mt-3">
                            <input data-parsley-dobvalidate data-parsley-required="true" class="form-control" name="date" value="" id="datetimepicker" type="text" placeholder="Date" required/>
                        </div>
                        <div class="form-group mt-3">
                            <input data-parsley-required="true" class="form-control" name="subject" type="text" placeholder="Subject" required/>
                        </div>
                        <div class="form-group mt-3">
                            <input data-parsley-type="number" data-parsley-required="true" class="form-control" name="amount" type="text" placeholder="Amount" required/>
                        </div>
                        <div class="form-group mt-3">
                            <input data-parsley-required="true" class="form-control" name="remark" type="text" placeholder="Remark" required/>
                        </div>
                        <div class="form-group mt-3 d-grid gap-2">
                            <button type="submit" class="btn btn-primary" id="btn-txt">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="card col-md-10 col-md-card shadow-lg p-3 mb-5 bg-white rounded">
                <div class="card-body table-responsive">
                    <table class="table table-sm table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Date</th>
                                <th>Subject</th>
                                <th>Amount</th>
                                <th>Remark</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center text-center">
            <div class="card col-md-4 col-md-card shadow-lg p-3 mb-5 bg-white rounded">
                <div id="total" class="card-body">
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center text-center">
            <div class="card">
                <div class="card-body">
                    <small class="text-muted">Author: Russel Rodrigues</small>
                </div>
            </div>
        </div>
       <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="datasaverform2" method="POST">
                        <input id="modalid" type="hidden" name="id" value=""/>
                        <div class="form-group mt-3">
                            <input data-parsley-dobvalidate data-parsley-required="true" id="modaldate" class="form-control" name="date" value="" type="text" placeholder="Date" required/>
                        </div>
                        <div class="form-group mt-3">
                            <input data-parsley-required="true" id="modalsubject" class="form-control" name="subject" type="text" placeholder="Subject" required/>
                        </div>
                        <div class="form-group mt-3">
                            <input data-parsley-type="number" data-parsley-required="true" id="modalamount" class="form-control" name="amount" type="text" placeholder="Amount" required/>
                        </div>
                        <div class="form-group mt-3">
                            <input data-parsley-required="true" id="modalremark" class="form-control" name="remark" type="text" placeholder="Remark" required/>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="submitedit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="datasaverform3" method="POST">
                        <input id="deleteid" type="hidden" name="id" value=""/>
                    </form>
                    Are you sure???
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" id="submitdelete" class="btn btn-primary">Yes</button>
                </div>
            </div>
            </div>
        </div>
    </div>  
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.11.4/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.0.0/parsley.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script src="js/index.js"></script>
</html>