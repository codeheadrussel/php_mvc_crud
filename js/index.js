$(document).ready(function() {
    window.ParsleyValidator.addValidator('dobvalidate', function (value, requirement)
    {
       var regExp = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
       return '' !== value ? regExp.test( value ) : false;
    }, 32)
   .addMessage('en', 'dobvalidate', 'Please enter date in yyyy-mm-dd format');
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    
    let datatableobj = {
        action : 'select'
    };
    function price_in_words(price) 
    {
        var sglDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"],
            dblDigit = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"],
            tensPlace = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"],
            handle_tens = function(dgt, prevDgt) {
            return 0 == dgt ? "" : " " + (1 == dgt ? dblDigit[prevDgt] : tensPlace[dgt])
            },
            handle_utlc = function(dgt, nxtDgt, denom) {
            return (0 != dgt && 1 != nxtDgt ? " " + sglDigit[dgt] : "") + (0 != nxtDgt || dgt > 0 ? " " + denom : "")
            };

        var str = "",
            digitIdx = 0,
            digit = 0,
            nxtDigit = 0,
            words = [];
        if (price += "", isNaN(parseInt(price))) str = "";
        else if (parseInt(price) > 0 && price.length <= 10) {
            for (digitIdx = price.length - 1; digitIdx >= 0; digitIdx--) switch (digit = price[digitIdx] - 0, nxtDigit = digitIdx > 0 ? price[digitIdx - 1] - 0 : 0, price.length - digitIdx - 1) {
            case 0:
                words.push(handle_utlc(digit, nxtDigit, ""));
                break;
            case 1:
                words.push(handle_tens(digit, price[digitIdx + 1]));
                break;
            case 2:
                words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] && 0 != price[digitIdx + 2] ? " and" : "") : "");
                break;
            case 3:
                words.push(handle_utlc(digit, nxtDigit, "Thousand"));
                break;
            case 4:
                words.push(handle_tens(digit, price[digitIdx + 1]));
                break;
            case 5:
                words.push(handle_utlc(digit, nxtDigit, "Lakh"));
                break;
            case 6:
                words.push(handle_tens(digit, price[digitIdx + 1]));
                break;
            case 7:
                words.push(handle_utlc(digit, nxtDigit, "Crore"));
                break;
            case 8:
                words.push(handle_tens(digit, price[digitIdx + 1]));
                break;
            case 9:
                words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] || 0 != price[digitIdx + 2] ? " and" : " Crore") : "")
            }
            str = words.reverse().join("")
        } else str = "";

        return str
    }
    let table = $('#example').DataTable( {
        "ajax":{
            "type"          : "POST",
            "url"           : 'controller/databasecontroller.php',
            "dataType"      : "json",
            "data"          : datatableobj,
            "dataSrc"       : function (json)
            {
                let totalamt = 0;
                console.log(json.data)
                for (let [key, value] of Object.entries(json.data)) {
                
                    console.log(value.amount);
                    totalamt = Number(totalamt) + Number(value.amount);
                    
                  }
                  let inwords = price_in_words(totalamt);
                  $('#total').html('<h5 class="display-4">Total Amount: </h5><h6 class="display-4">'+ totalamt+' Rs</h6><p class="lead">'+inwords+' Rs</p>');
                return json.data;
            },
            error       : function ()
            {
            $.notifyBar({cssClass: "error", html: "Error loading data from server."});
            }
        },
        "dom": 'B<"mt-2"l><"mt-4"f><"mt-4"t><"mt-4"i><"mt-4"p>',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        buttons: {
            dom: {
                button: {
                    tag: 'button',
                    className: 'btn btn-primary btn-sm mb-2'
                },
                buttonLiner: {
                    tag: null
                }
            }
        },
        responsive: true
    } );
    $('#btn-txt').click( function (e)
    {
        var f = $("#datasaverform");
        f.parsley().validate();
        e.preventDefault();
        if (f.parsley().isValid())
        {
            $.ajax(
            {
                url             : 'controller/databasecontroller.php',
                type            : "POST",
                data            :  $('#datasaverform').serialize()+'&action=insert',
                dataType        : 'json',
                encode          : true,
                beforeSend      : function ()
                {
                    $.blockUI({message: '<i class="fas fa-spinner position-left" style="font-size:21px"></i>'});
                },
                success         : function (data)
                {
                    
                    $.unblockUI();
                    if (data.status == 'success')
                    {
                        $.notifyBar({cssClass: "success", html: data.html_message});
                        table.ajax.reload();

                    }
                    else
                    {
                        $.notifyBar({cssClass: "error", html: data.html_message});
                    }
                },
                error           : function (data, errorThrown)
                {
                    $.unblockUI();
                    $.notifyBar({cssClass: "error", html: "Error occured!"});
                }
            });
        }
    });

    $('#submitdelete').click(function ()
    {
        $.ajax(
        {
            url             : 'controller/databasecontroller.php',
            type            : "POST",
            data            :  $('#datasaverform3').serialize()+'&action=delete',
            dataType        : 'json',
            encode          : true,
            beforeSend      : function ()
            {
                $.blockUI({message: '<i class="fas fa-spinner position-left" style="font-size:21px"></i>'});
            },
            success         : function (data)
            {
                $.unblockUI();
                if (data.status == 'success')
                {
                    $.notifyBar({cssClass: "success", html: data.html_message});
                    table.ajax.reload();
                    $('#exampleModal2').modal('hide');
                }
                else
                {
                    $.notifyBar({cssClass: "error", html: data.html_message});
                }
            },
            error           : function (data, errorThrown)
            {
                $.unblockUI();
                $.notifyBar({cssClass: "error", html: "Error occured!"});
            }
        });
    });

    $('#submitedit').click(function()
    {
        var f2 = $("#datasaverform2");
        f2.parsley().validate();
        if (f2.parsley().isValid())
        {
            $.ajax(
            {
                url             : 'controller/databasecontroller.php',
                type            : "POST",
                data            :  $('#datasaverform2').serialize()+'&action=edit',
                dataType        : 'json',
                encode          : true,
                beforeSend      : function ()
                {
                    $.blockUI({message: '<i class="fas fa-spinner position-left" style="font-size:21px"></i>'});
                },
                success         : function (data)
                {
                    $.unblockUI();
                    if (data.status == 'success')
                    {
                        $.notifyBar({cssClass: "success", html: data.html_message});
                        table.ajax.reload();
                        $('#exampleModal').modal('hide');
                        
                    }
                    else
                    {
                        $.notifyBar({cssClass: "error", html: data.html_message});
                    }
                },
                error           : function (data, errorThrown)
                {
                    $.unblockUI();
                    $.notifyBar({cssClass: "error", html: "Error occured!"});
                }
            });
        }
    });
} );
function editdata(id)
{
    $.ajax(
    {
        url             : 'controller/databasecontroller.php',
        type            : "POST",
        data            :  {'action' : 'selectid', 'id' : id},
        dataType        : 'json',
        encode          : true,
        beforeSend      : function ()
        {
            $.blockUI({message: '<i class="fas fa-spinner position-left" style="font-size:21px"></i>'});
        },
        success         : function (data)
        {
            console.log(typeof data);
            for (let [key, value] of Object.entries(data)) {
                
                console.log(key + ':' + value);
                $('#modal'+key).val(value);
              }
            $.unblockUI();
            // if (data.status == 'success')
            // {
            //     $.notifyBar({cssClass: "success", html: data.html_message});
            // }
            // else
            // {
            //     $.notifyBar({cssClass: "error", html: data.html_message});
            // }
        },
        error           : function (data, errorThrown)
        {
            $.unblockUI();
            $.notifyBar({cssClass: "error", html: "Error occured!"});
        }
    });
}
function deletedata(id)
{
    $('#deleteid').val(id);
}



