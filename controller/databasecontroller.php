
<?php
require '../model/Database.php';
if($_POST)
{
    $action = $_POST['action'];

    if($action == 'insert')
    {
        $date       = $_POST['date'];
        $subject    = $_POST['subject'];
        $amount     = $_POST['amount'];
        $remark     = $_POST['remark'];
        $db       = new Database();
        $insert   = $db->insert('info',array('date' => $date, 'subject' => $subject, 'amount' => $amount, 'remark' => $remark));    
        echo json_encode($insert);
        die;
    }
    elseif($action == 'select')
    {
        $db         = new Database();
        $result     = $db->select('info');
        echo json_encode($result);
        die; 
    }
    elseif($action == 'selectid')
    {
        $id = $_POST['id'];
        $db = new Database();
        $result = $db->selectid($id,'info');
        echo json_encode($result);
        die;
    }
    elseif($action == 'edit')
    {
        $date       = $_POST['date'];
        $subject    = $_POST['subject'];
        $amount     = $_POST['amount'];
        $remark     = $_POST['remark'];
        $id         = $_POST['id'];
        $db       = new Database();
        $result   = $db->edit('info',array('date' => $date, 'subject' => $subject, 'amount' => $amount, 'remark' => $remark),"id=$id");    
        echo json_encode($result);
        die;
    }
    elseif($action == 'delete')
    {
        $id = $_POST['id'];
        $db = new Database();
        $result = $db->delete($id,'info');
        echo json_encode($result);
        die;
    }
}